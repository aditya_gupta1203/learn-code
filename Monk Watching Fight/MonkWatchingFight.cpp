#include <iostream>
using namespace std;

struct node
{
    int data;
    node *left;
    node *right;
};

	struct node* createNode(int value) {
    node* newNode;
    newNode = new node;
    newNode->data = value;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
	}
	
	struct node* insertDataIntoBst (node* root, int value) {
		if (root == NULL) {
			return createNode(value);
		}
		if (value <= root->data) {
			root->left = insertDataIntoBst(root->left, value);
		}
		else {
			root->right = insertDataIntoBst(root->right, value);
		}
		return root;
	}
	
	int DepthOfTree (node* root) {
		int leftTree = 0, rightTree = 0;
		if (root == NULL) {
			return 0;
		}
		
		leftTree = DepthOfTree(root->left);
		rightTree = DepthOfTree(root->right);
		
		if (leftTree >= rightTree) {
			return leftTree + 1;
		}
		else {
			return rightTree + 1;
		}
	}

int main() {
	int noOfNodes, dataToInsert;
	cin>>noOfNodes;
	struct node* rootNode = NULL;
	for(int index = 0; index < noOfNodes; index ++) {
		cin>>dataToInsert;
		rootNode = insertDataIntoBst(rootNode, dataToInsert);		
	}
	cout<<DepthOfTree(rootNode);
}
