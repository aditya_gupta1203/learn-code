#include <iostream>
using namespace std;

struct node {
	int data;
	node *left;
	node *right;
};

class ConvertingArrayIntoBst {
	public :
		struct node* createNode(int value) {
			node* newNode;
			newNode = new node;
			newNode->data = value;
			newNode->left = NULL;
			newNode->right = NULL;
			return newNode;
		}

		struct node* insertDataIntoBst (node* root, int value) {
			if (root == NULL) {
				return createNode(value);
			}
			if (value <= root->data) {
				root->left = insertDataIntoBst(root->left, value);
			} else {
				root->right = insertDataIntoBst(root->right, value);
			}
			return root;
		}

		int DepthOfTree (node* root) {
			int leftTreeDepth = 0, rightTreeDepth = 0;
			if (root == NULL) {
				return 0;
			}

			leftTreeDepth = DepthOfTree(root->left);
			rightTreeDepth = DepthOfTree(root->right);

			if (leftTreeDepth >= rightTreeDepth) {
				return leftTreeDepth + 1;
			} else {
				return rightTreeDepth + 1;
			}
		}
};

int main() {
	int noOfNodes, dataToInsert;
	cin>>noOfNodes;
	struct node* rootNode = NULL;
	ConvertingArrayIntoBst binarySearchTree;
	for(int index = 0; index < noOfNodes; index ++) {
		cin>>dataToInsert;
		rootNode = binarySearchTree.insertDataIntoBst(rootNode, dataToInsert);
	}
	cout<<binarySearchTree.DepthOfTree(rootNode);
}
