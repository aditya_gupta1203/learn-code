#include "stdafx.h"
#include "CppUnitTest.h"
#include "../BST/BST.h"

namespace BSTUnitTesting
{ 
 TEST_CLASS(ConvertingArrayIntoBst)
 {
 public:
 
 TEST_METHOD(createNode)
 {
 int value = 2;
 ConvertingArrayIntoBst bstTesting;
 Assert::AreEqual(2, bstTesting.root->value); 
 }
 TEST_METHOD(insertDataIntoBst)
 {
 ConvertingArrayIntoBst bstTesting;
 Assert::AreEqual(7, bstTesting.insertDataIntoBst(NULL, 7))
 }

 TEST_METHOD(DepthOfTree)
 {
 ConvertingArrayIntoBst bstTesting;
 node *root = NULL;
 root = bstTesting.insertDataIntoBst(root, 8);
 root = bstTesting.insertDataIntoBst(root, 1);
 root = bstTesting.insertDataIntoBst(root, 5);
 root = bstTesting.insertDataIntoBst(root, 9);
 Assert::AreEqual(3, bstTesting.DepthOfTree(root));
 } 
 };
}
