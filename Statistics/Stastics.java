import java.util.*;
import java.io.*;

class Statistics {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static int countForUniqueSportOccurance, sportLikedByMaximumParticipants = 0, footballLikers = 0;
	static String nameOfSportLikedMost = "";
	static HashMap<String,Integer> nameMap = new HashMap<String,Integer>();
    static HashMap<String,Integer> sportMap = new HashMap<String,Integer>();
	
	public static void main(String args[] ) throws Exception {
		int numberOfParticipantsToSurvey = Integer.parseInt(br.readLine());
		for(int index =0; index < numberOfParticipantsToSurvey; index ++){
			String[] storedNameAndSport = getValidInputFromParticipant();
			evaluateMostLikedSport(storedNameAndSport);
		}
		showMostLikedSportAndNumberOfFootballLovers();
	}
	
	public static String[] getValidInputFromParticipant() throws Exception {
		String ReadingLineEnteredByParticipant = br.readLine();
        String[] storingNameAndSport = ReadingLineEnteredByParticipant.trim().split(" ");
		
		if(!nameMap.containsKey(storingNameAndSport[0])) {
			countForUniqueSportOccurance = 1;
			nameMap.put(storingNameAndSport[0],1);
			if(!sportMap.containsKey(storingNameAndSport[1])) {
				sportMap.put(storingNameAndSport[1],1);
			}
			else {
				countForUniqueSportOccurance = sportMap.get(storingNameAndSport[1])+1;
                sportMap.put(storingNameAndSport[1],countForUniqueSportOccurance);
			}    
		}
		return storingNameAndSport;
	}
	
	public static void evaluateMostLikedSport(String[] nameAndSportStored) {
		if(nameAndSportStored[1].equals("football"))
			footballLikers ++;
		if(countForUniqueSportOccurance > sportLikedByMaximumParticipants) {
			nameOfSportLikedMost = nameAndSportStored[1];
			sportLikedByMaximumParticipants = countForUniqueSportOccurance;
		}
		else if (countForUniqueSportOccurance == sportLikedByMaximumParticipants) {
			if(nameOfSportLikedMost.compareTo(nameAndSportStored[1]) > 1)
				nameOfSportLikedMost = nameAndSportStored[1];
		} 
	}
	
	public static void showMostLikedSportAndNumberOfFootballLovers() {
		System.out.println(nameOfSportLikedMost);
        System.out.println(footballLikers);		
	}
}