#include <iostream>
using namespace std;
 
struct node
{
    int popularity;
    node *nextFriend;
    node *prevFriend;
};
class RemoveFriends {
  private :
  int currentFriends,friendsToDelete;
  bool deleteFriend = false;
  struct node* head=NULL, *last=NULL, *selectedPopularityFromList, *currentPopularity;
  public :
  void getFriendList() {
  	cin>>currentFriends>>friendsToDelete;
      for(int index=0;index<currentFriends;index++){
          currentPopularity = new node;
          cin>>currentPopularity->popularity;
          currentPopularity->nextFriend=NULL;
          currentPopularity->prevFriend=NULL;
          if(head==NULL)
          {
          	head=currentPopularity;
          	last=currentPopularity;
          }
          else
          {
              last->nextFriend=currentPopularity;
              currentPopularity->prevFriend=last;
          }
          last=currentPopularity;
      }
  }
  void removeFriends() {
  	selectedPopularityFromList= new node;
  	selectedPopularityFromList=head;
  	  while(friendsToDelete) {
  		if(selectedPopularityFromList->nextFriend==NULL)
  		break;
  		if((selectedPopularityFromList->popularity)<(selectedPopularityFromList->nextFriend->popularity)) {
  			if(selectedPopularityFromList->prevFriend==NULL) {
  				head=head->nextFriend;
  				head->prevFriend=NULL;
  				selectedPopularityFromList=head;
  				--friendsToDelete;
  				deleteFriend=true; }
			else {
			    selectedPopularityFromList->prevFriend->nextFriend=selectedPopularityFromList->nextFriend;
			    selectedPopularityFromList->nextFriend->prevFriend=selectedPopularityFromList->prevFriend;
			    selectedPopularityFromList=selectedPopularityFromList->prevFriend;
			 	--friendsToDelete;
			  	deleteFriend=true; }
		}
		else
			selectedPopularityFromList=selectedPopularityFromList->nextFriend;
      }
  }
  void checkIfNoFriendsRemoved() {
      if(deleteFriend==false) {
      	  last=last->prevFriend;
          last->nextFriend=NULL;
	  }
  }
  void displayRemainingFriends() {
      while(head!=NULL) {
  		cout<<head->popularity<<" ";
  		head=head->nextFriend;
	  }
	  cout<<"\n";
  }
};
 
int main()
{
    int testCases;
    cin>>testCases;
    while (testCases--) {
        RemoveFriends friendRemoval;
        friendRemoval.getFriendList();
        friendRemoval.removeFriends();
        friendRemoval.checkIfNoFriendsRemoved();
        friendRemoval.displayRemainingFriends();
    }
    return 0;
}
