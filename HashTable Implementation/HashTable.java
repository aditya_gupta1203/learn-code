
public class HashTable {

	public static void main(String[] args) {
		HashTableImplementation<Integer, Integer> hashMapImplement = new HashTableImplementation<Integer, Integer>();
		hashMapImplement.put(9, 100);
		hashMapImplement.put(7, 163);
		hashMapImplement.put(12, 214);
		hashMapImplement.put(15, 196);
		hashMapImplement.put(26, 314);

        System.out.println("value at key 7 = "
                     + hashMapImplement.get(7));

        System.out.println("Displaying all data  : ");
        hashMapImplement.display();
        System.out.println();
        if(hashMapImplement.remove(12)) {
           System.out.println("Removing value associated with key 12");
        }else {
            System.out.println("Something went wrong while Removing value associated with key 12");
        }
        System.out.println("Displaying all data : ");
        hashMapImplement.display();
	}
}
