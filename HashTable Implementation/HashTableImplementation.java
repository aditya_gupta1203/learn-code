import java.util.*;

public class HashTableImplementation<K, V> implements MyMap<K, V>{
  LinkedList<MyMap.Entry<K,V>>[] dataBucket;
  private int capacity = 4;
  private int size = 0;

  public HashTableImplementation() {
	  dataBucket = new LinkedList[trimToPowerOf2(capacity)];
  }

  public V put(K key, V value) {
	if (get(key) != null) {
	  int bucketIndex = getHashIndex(key.hashCode());
	  LinkedList<Entry<K, V>> bucket = dataBucket[bucketIndex]; 
	  for (Entry<K, V> entry: bucket)
	    if (entry.getKey().equals(key)) {
	    	V oldValue = entry.getValue();
	    	entry.value = value; 
	    	return oldValue;
	    }
	}
	int bucketIndex = getHashIndex(key.hashCode());
	if (dataBucket[bucketIndex] == null) {
		dataBucket[bucketIndex] = new LinkedList<Entry<K, V>>();
	}
	dataBucket[bucketIndex].add(new MyMap.Entry<K, V>(key, value));
	size++; 	
	return value;  
  } 

  public V get(K key) {
    int bucketIndex = getHashIndex(key.hashCode());
    if (dataBucket[bucketIndex] != null) {
    	LinkedList<Entry<K, V>> bucket = dataBucket[bucketIndex]; 
    	for (Entry<K, V> entry: bucket) {
    		if (entry.getKey().equals(key)) {
    			return entry.getValue();
    		}
    	}
    }
    
    return null;
  }
  
  public boolean remove(K key) {
    int bucketIndex = getHashIndex(key.hashCode());
    if (dataBucket[bucketIndex] != null) {
    	LinkedList<Entry<K, V>> bucket = dataBucket[bucketIndex]; 
    	for (Entry<K, V> entry: bucket) {
    		if (entry.getKey().equals(key)) {
    			bucket.remove(entry);
    			size--; 
    			break; 
    		}
    	}
    	return  true;
    }
    return false;
}

  public void removeEntries() {
    for (int i = 0; i < trimToPowerOf2(capacity); i++) {
      if (dataBucket[i] != null) {
    	  dataBucket[i].clear();
      }
    }
  }
  
  public void display() {
	for (int i = 0; i < capacity; i++) {
		if (dataBucket[i] != null) {
			LinkedList<Entry<K, V>> bucket = dataBucket[i];
			for (Entry<K, V> entry: bucket) {
				System.out.print("{" + entry.key + "=" + entry.value + "}" + " ");
			}
		}
	}
	}
  
  public int trimToPowerOf2(int initialCapacity) {
    int capacity = 1;
    while (capacity < initialCapacity) {
      capacity <<= 1;
    }
    
    return capacity;
  }

  public int getHashIndex(int hashCode) {
    return hashCode & (capacity - 1);
  }
}
