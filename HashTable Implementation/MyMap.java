
public interface MyMap<K, V> {

	class Entry<K, V> {
    	K key;
        V value;
        Entry next;
 
        Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
 
        public V getValue() {
            return value;
        }
 
        public void setValue(V value) {
            this.value = value;
        }
 
        public K getKey() {
            return key;
        }
    } 
}
